﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Calculate : MonoBehaviour
{
    int firstValue, secondValue, temp, answer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void sumar() {
        SceneManager.LoadScene(1);
    }

    public void restar() {
        SceneManager.LoadScene(2);
    }

    public void multiplicar() {
        SceneManager.LoadScene(3);
    }

    public void dividir() {
        SceneManager.LoadScene(4);
    }
}
