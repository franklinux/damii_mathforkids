﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class DividirGetBack : MonoBehaviour
{
    public GameObject firstNumber;
    public GameObject secondNumber;
    public GameObject answerObject;
    public GameObject resultText;
    int firstValue, secondValue, answer, answerInput;

    void Start()
    {
        generateNewNumbers();
    }

    void generateNewNumbers()
    {
        do
        {
            firstValue = Random.Range(4, 100);
            secondValue = Random.Range(2, 100);

        } while (firstValue % secondValue != 0 || firstValue == secondValue || secondValue > firstValue);

        answer = firstValue / secondValue;
        firstNumber.GetComponent<TextMeshProUGUI>().text = firstValue.ToString();
        secondNumber.GetComponent<TextMeshProUGUI>().text = secondValue.ToString();
    }

    void Update()
    {
        int.TryParse(answerObject.GetComponent<TMP_InputField>().text, out answerInput);


        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (answerInput == answer)
            {
                resultText.GetComponent<TextMeshProUGUI>().text = "¡Correcto!";
            }
            else
            {
                resultText.GetComponent<TextMeshProUGUI>().text = "¡Incorrecto!";
            }
        }
    }

    public void getBack()
    {
        SceneManager.LoadScene(0);
    }

    public void next()
    {
        generateNewNumbers();
        answerObject.GetComponent<TMP_InputField>().text = "";
        resultText.GetComponent<TextMeshProUGUI>().text = "Resultado";
    }
}
