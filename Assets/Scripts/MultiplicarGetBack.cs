﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MultiplicarGetBack : MonoBehaviour
{
    public GameObject firstNumber;
    public GameObject secondNumber;
    public GameObject answerObject;
    public GameObject resultText;
    int firstValue, secondValue, answer, answerInput;

    void Start()
    {
        generateNewNumbers();
    }

    void generateNewNumbers()
    {
        firstValue = Random.Range(1, 12);
        secondValue = Random.Range(1, 12);
        answer = firstValue * secondValue;
        firstNumber.GetComponent<TextMeshProUGUI>().text = firstValue.ToString();
        secondNumber.GetComponent<TextMeshProUGUI>().text = secondValue.ToString();
    }

    void Update()
    {
        int.TryParse(answerObject.GetComponent<TMP_InputField>().text, out answerInput);


        if (Input.GetKeyDown(KeyCode.Return))
        {
            if (answerInput == answer)
            {
                resultText.GetComponent<TextMeshProUGUI>().text = "¡Correcto!";
            }
            else
            {
                resultText.GetComponent<TextMeshProUGUI>().text = "¡Incorrecto!";
            }
        }
    }

    public void getBack()
    {
        SceneManager.LoadScene(0);
    }

    public void next()
    {
        generateNewNumbers();
        answerObject.GetComponent<TMP_InputField>().text = "";
        resultText.GetComponent<TextMeshProUGUI>().text = "Resultado";
    }
}
